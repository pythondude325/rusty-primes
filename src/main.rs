use std::time::Instant;
use std::collections::BTreeSet;

fn is_prime(num: u64, prime_list: &[u64]) -> bool {
    let root: u64 = (num as f64).sqrt() as u64;

    prime_list.iter().take_while(|&c| c < &root).find(|&c| num % c == 0).is_none()
}

fn main(){
    let mut prime_list: Vec<u64> = vec![2];

    let mut current_test_number: u64 = 3;
    let mut timer = Instant::now();

    let start = Instant::now();

    while prime_list.len() < 2_000_000 {
        if is_prime(current_test_number, &prime_list) {
            prime_list.push(current_test_number);

            if prime_list.len() % (1<<16) == 0 {
                let duration = timer.elapsed();
                println!("{} ms", duration.as_millis());
                timer = Instant::now();
            }

        }
        current_test_number += 2;
    }

    println!("Total time: {} ms", start.elapsed().as_millis());

}


/*fn main(){
    let mut prime_list: BTreeSet<u64> = BTreeSet::new();
		prime_list.insert(2);

    let mut current_test_number: u64 = 3;
    let mut timer = Instant::now();

    let start = Instant::now();

    while prime_list.len() < 2_000_000 {
        if is_prime(current_test_number, &prime_list) {
            prime_list.insert(current_test_number);

            if prime_list.len() % 1<<16 == 0 {
                let duration = timer.elapsed();
                println!("{} ms", duration.as_millis());
                timer = Instant::now();
            }
        }
        current_test_number += 2;
    }

    println!("Total time: {} ms", start.elapsed().as_millis());

}*/

/*
fn integer_sqrt(a: i32) -> i32 {
    (a as f64).sqrt() as i32
}

struct PrimeIterator {
    primes: BTreeSet<i32>
}

impl PrimeIterator {
    fn new() -> PrimeIterator {
        PrimeIterator{ primes: BTreeSet::new() }
    }
}

impl Iterator for PrimeIterator {
    type Item = i32;

    fn next(&mut self) -> Option<i32> {
        let new_prime = (self.primes.iter().last().unwrap_or(&1)+1..).find(|num|{
            self.primes.range(..=integer_sqrt(*num)).find(|&prime| num % prime == 0).is_none()
        });
        
        if let Some(prime) = new_prime {
            self.primes.insert(prime);
        }
        
        new_prime
    }
}


fn main(){
    let start = Instant::now();

		println!("{}", PrimeIterator::new().nth(2_000_000).unwrap());

    println!("Total time: {} ms", start.elapsed().as_millis());

}*/